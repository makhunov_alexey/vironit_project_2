const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

/* GET home page. */
router.get('/', function (req, res, next) {
    const user = {};
    res.render('register', {title: 'Food Delivery service', user: user});
});

router.use(require('../controllers/tokenVerification'));
router.all(require('../controllers/tokenVerification'));
router.get('/index', function (req, res, next) {
    res.render('menu');
});
router.all(function (req, res, next) {
    var user = userController.get_user(req);
    res.user = user;
    next();
});

module.exports = router;
