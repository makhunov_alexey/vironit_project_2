const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const adminController = require('../controllers/adminController');
const contentController = require('../controllers/contentController');
const jwt = require('jsonwebtoken');
const config = require('./../config/config');
const User = require('../models/userModel');
const {Category} = require('../models/categoryModel');
const {Dish} = require('../models/categoryModel');
const Order = require('../models/orderModel');
const Comment = require('../models/commentModel');


// const appfunction = require('../app');
const multer = require('multer');

const multerConf = {
    storage: multer.diskStorage({
        destination: function (req, file, next) {
            next(null, './public/images')
        },
        filename: function (req, file, next) {
            const ext = file.mimetype.split('/')[1];
            next(null, file.fieldname + '-' + Date.now() + '.' + ext);
        }
    }),
    fileFilter: function (req, file, next) {
        if (!file) {
            next();
        }
        const image = file.mimetype.startsWith('image/');
        if (image) {
            next(null, true);
        } else {
            next({message: 'File type not supported'}, false)
        }
    }
};

router.get('/login', async function (req, res, next) {
    // let token = req.cookies["accessToken"];
    // const {_id} = jwt.decode(token);
    // const user = await User.findById(_id);
    let user = {};
    res.render('login', {title: 'hello', user: user});
});

router.get('/menu', async function (req, res) {
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const user = await User.findById(_id);
    await jwt.verify(token, config.tokens.secret, async function (err, decoded) {
        if (err && (user !== null)) {
            config.client.del(user._id.toString());
            res.clearCookie("accessToken");
            res.render('loginrefresh');
        }
        if(decoded){
            // const user = await jwt.verify(token, config.tokens.secret);
            if (user.isConfirmed) {
                let categories = await Category.find().populate({ path: 'categoryLikes', select: '_user'});
                res.render("menu", {categories: categories, user: user, userId: user._id});

            }
        }
    });
});

router.get('/dishes/:id', contentController.showDishes);

router.get('/admin', async function (req, res) {
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const user = await User.findById(_id);
    await jwt.verify(token, config.tokens.secret, async function (err, decoded) {
        if (err) {
            const {_id} = jwt.decode(token);
            const user = await User.findById(_id);
            config.client.del(user._id.toString());
            res.clearCookie("accessToken");
            res.render('loginrefresh');
        }
        if(decoded){
            const user = await jwt.verify(token, config.tokens.secret);
            if (user.isConfirmed  && user.role === 'Admin') {
                Category.find()
                    .then(docs => {
                        res.render('admin', {title: 'hello', user: user});
                    });
            }
            else {
                res.render('login');
            }
        }
    });
});

router.get('/admin/menuEdit', async function (req, res) {
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const user = await User.findById(_id);
    await jwt.verify(token, config.tokens.secret, async function (err, decoded) {
        if (err) {
            const {_id} = jwt.decode(token);
            const user = await User.findById(_id);
            config.client.del(user._id.toString());
            res.clearCookie("accessToken");
            res.render('loginrefresh');
        }
        if(decoded){
            const user = await jwt.verify(token, config.tokens.secret);
            if (user.isConfirmed && user.role === 'Admin') {
                Category.find()
                    .then(docs => {
                        res.render("menuEdit", {docs: docs, user: user});
                    });
            } else {
                res.render('login');
            }
        }
    });
});

router.get('/admin/dishesOptions', async function (req, res) {
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const user = await User.findById(_id);
    await jwt.verify(token, config.tokens.secret, async function (err, decoded) {
        if (err) {
            const {_id} = jwt.decode(token);
            const user = await User.findById(_id);
            config.client.del(user._id.toString());
            res.clearCookie("accessToken");
            res.render('loginrefresh');
        }
        if(decoded){
            const user = await jwt.verify(token, config.tokens.secret);
            if (user.isConfirmed && user.role === 'Admin') {
                const dishes = await Dish.find().populate({ path: '_category', select: 'categoryName' });
                let errors = {};
                let validation = {};

                Category.find()
                    .then(docs => {
                        res.render("dishesOptions", {docs: docs, dishes: dishes, user: user, errors: errors, validation:validation});
                    });
            }
            else {
                res.render('login');
            }
        }
    });
});

router.get('/admin/ordersOptions', async function (req, res) {
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const user = await User.findById(_id);
    await jwt.verify(token, config.tokens.secret, async function (err, decoded) {
        if (err) {
            const {_id} = jwt.decode(token);
            const user = await User.findById(_id);
            config.client.del(user._id.toString());
            res.clearCookie("accessToken");
            res.render('loginrefresh');
        }
        if(decoded){
            const user = await jwt.verify(token, config.tokens.secret);
            if (user.isConfirmed && user.role === 'Admin') {
                const orders = await Order.find().populate({ path: 'dishes', select: 'itemTitle'}).populate({ path: '_user', select: 'email' });
                const dishes = await Dish.find().sort('ordersCount');
                res.render("ordersOptions", {orders: orders, user: user});
            }
            else {
                res.render('login');
            }
        }
    });
});

router.get('/admin/commentsOptions', async function (req, res) {
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const user = await User.findById(_id);
    await jwt.verify(token, config.tokens.secret, async function (err, decoded) {
        if (err) {
            const {_id} = jwt.decode(token);
            const user = await User.findById(_id);
            config.client.del(user._id.toString());
            res.clearCookie("accessToken");
            res.render('loginrefresh');
        }
        if(decoded){
            const user = await jwt.verify(token, config.tokens.secret);
            if (user.isConfirmed && user.role === 'Admin') {

            const categories = await Category.find().populate({ path: 'dishes', select: 'comments itemTitle image', populate: { path: 'comments', populate: { path: '_user', select: 'email'}}});
                res.render("commentsOptions", {categories: categories, user: user});
            }
            else {
                res.render('login');
            }
        }
    });
});

router.get('/register', async function (req, res) {
    const user = {};
    res.render("register", {title: "Registration page", user: user});
});

router.get('/confirmed', async function (req, res, next) {
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const user = await User.findById(_id);
    res.render('confirmed', {user: user});
});

router.get('/verify', userController.verify);

router.get('/logout', async function (req, res) {
    let token = req.cookies["accessToken"];
    await jwt.verify(token, config.tokens.secret, async function (err, decoded) {
        if (err) {
           console.log(err);
            res.redirect('back');
        }
        if(decoded) {
            const {_id} = jwt.decode(token);
            // let user = await User.findById(_id);
            config.client.del(_id.toString());
            res.clearCookie("accessToken");
            let user = {};
            res.render('login', {user: user});
        }
    });
});

router.get('/calendar', async function (req, res) {
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const user = await User.findById(_id);
    await jwt.verify(token, config.tokens.secret, async function (err, decoded) {
        if (err) {
            const {_id} = jwt.decode(token);

            config.client.del(_id.toString());
            res.clearCookie("accessToken");
            res.render('loginrefresh');
        }
        if(decoded){
            const {_id} = jwt.decode(token);
            const user = await User.findById(_id);
            if (user.isConfirmed) {
                const dishes = await Dish.find().populate({ path: '_category', select: 'categoryName' });
                const orders = await Order.find({ _user: _id}, 'dishes orderDate').populate({ path: 'dishes' });
                Category.find()
                    .then(docs => {
                        res.render("calendar", {docs: docs, dishes: dishes, orders: orders, user: user});
                    });
            }
        }
    });
});

router.get('/dashboard', contentController.showDashBoard);

router.get('/loginrefresh', async function (req, res) {
    let token = req.cookies["accessToken"];
    await jwt.verify(token, config.tokens.secret, async function (err, decoded) {
            const {_id} = jwt.decode(token);
            const user = await User.findById(_id);
            config.client.del(user._id.toString());
            res.clearCookie("accessToken");
            res.render('loginrefresh');
    });
});

router.post('/login', userController.authenticate);
router.post('/register', userController.register);

router.post('/admin/addCategory', adminController.addCategory);
router.post('/admin/renameCategory/:id', adminController.renameCategory);
router.delete('/admin/deleteCategory/:id', adminController.deleteCategory);

router.post('/admin/addDish', multer(multerConf).single('photo'), adminController.addDish);
router.post('/admin/renameDish/:id',  multer(multerConf).single('editPhoto'), adminController.renameDish);
router.post('/admin/deleteDish/:id', adminController.deleteDish);

router.post('/createOrder', userController.createOrder);
router.delete('/deleteOrder/:id', userController.deleteOrder);

router.post('/addComment/:id', userController.addComment);
router.post('/editComment/:id', userController.updateComment);
router.delete('/deleteComment/:id', userController.deleteComment);
router.post('/addDishLike/:id', userController.addDishLike);
router.post('/addCategoryLike/:id', userController.addCategoryLike);

module.exports = router;
