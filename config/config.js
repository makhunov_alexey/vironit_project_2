const mongoose = require('mongoose');
const redis = require('redis');
const dotenv = require('dotenv');
dotenv.config();

mongoose.connect(String(process.env.DB), {useNewUrlParser:true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log("Database connected");
});

const tokens = {
    "secret": "secret-access-token",
    "refreshTokenSecret": "secret-refresh-token",
    "tokenLife": 600,
    "refreshTokenLife": 86000
};

//Create Redis Client
let client = redis.createClient();

client.on('connect', function () {
    console.log('ok')
});

client.on('error', function (err) {
    console.log('err' + err)
});


module.exports = {
    mongoose: mongoose,
    tokens: tokens,
    client: client,
    db: db
};