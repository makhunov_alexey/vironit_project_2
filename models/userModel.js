const config = require('./../config/config');
const Schema = config.mongoose.Schema;

const userSchema = new Schema({
    email: String,
    password: String,
    role: String,
    isConfirmed: Boolean,
    orders: [{type: Schema.ObjectId, ref: 'order'}],
    comments: [{type: Schema.ObjectId, ref: 'comment'}],
    dishLikes: [{type: Schema.ObjectId, ref: 'dishLike'}],
    categoryLikes: [{type: Schema.ObjectId, ref: 'categoryLike'}]
}, {
    timestamps: {
        createdAt: 'createdAt',
        updatedAt: 'updatedAt'
    }
});

const User = config.mongoose.model("user", userSchema);
module.exports = User;

