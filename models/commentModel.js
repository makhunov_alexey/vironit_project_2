const config = require('./../config/config');
const Schema = config.mongoose.Schema;

const commentSchema = new Schema({
        _user: {type: Schema.ObjectId, ref: 'user'},
        _dish: {type: Schema.ObjectId, ref: 'dish'},
        commentBody: {
            type: String,
            default: 'Comment'
        }
    },
    {
        timestamps: {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt'
        }
    });

const Comment = config.mongoose.model("comment", commentSchema);


module.exports = Comment;
