const config = require('./../config/config');
const Schema = config.mongoose.Schema;

const orderSchema = new Schema({
    _user: {type: Schema.ObjectId, ref: 'user'},
    dishes: [{type: Schema.ObjectId, ref: 'dish'}]
    ,
    orderDate: {
        type: Date
    }
});

const Order = config.mongoose.model("order", orderSchema);


module.exports = Order;
