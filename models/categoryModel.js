const config = require('./../config/config');
const Schema = config.mongoose.Schema;
const Comment = require('../models/commentModel');
const DishLike = require('../models/dishLikeModel');
const CategoryLike = require('../models/categoryLikeModel');
const User = require('../models/userModel');
const Order = require('../models/orderModel');





const categorySchema = new Schema({
        categoryName: {
            type: String,
            default: 'Category Name'
        },
        categoryLikes: [{type: Schema.ObjectId, ref: 'categoryLike'}],
        dishes: [{type: Schema.ObjectId, ref: 'dish'}]
    },
    {
        timestamps: {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt'
        }
    });


const dishSchema = new Schema({
    _category: {type: Schema.ObjectId, ref: 'category'},
    comments: [{type: Schema.ObjectId, ref: 'comment'}],
    dishLikes: [{type: Schema.ObjectId, ref: 'dishLike'}],
    ordersCount: Number,
    orders: [{type: Schema.ObjectId, ref: 'order'}],
    itemTitle: {
        type: String,
        default: 'Title'
    },
    itemDescription: {
        type: String,
        default: 'Description'
    },
    itemCost: {
        type: Number,
        default: 0
    },
    image: {
        type: String,
        default: 'plate2.jpg'
    }
});
dishSchema.pre('validate', function (next) {
    this.ordersCount = this.orders.length
    next();
});
const removeLinkedCommentsDishLikes = async (doc)=> {
    doc.comments.forEach(async(element) => {
        await Comment.findOneAndDelete({'_id': element});
    }); 
    doc.dishLikes.forEach(async(element) => {
        await DishLike.findOneAndDelete({'_id': element}); 
    });
    doc.orders.forEach(async(element) => {
        await Order.findById( element,function(err,result){
            result.dishes.splice(result.dishes.indexOf(element),1);
            if(result.dishes.length!=0){
                result.save();
            }else result.remove();
        }); 
    });
};
dishSchema.post('findOneAndDelete', removeLinkedCommentsDishLikes);

const removeLinkedDocuments= (doc)=> {
    doc.dishes.forEach(async(element) => {
        await Dish.findOneAndDelete({'_id': element});
    });
    doc.categoryLikes.forEach(async(element)=>{
        await CategoryLike.findByIdAndDelete({'_id':element},async function(err,result){
            await User.findById(result._user,function(err,res){
                res.categoryLikes.splice(res.categoryLikes.indexOf(element),1);
                res.save();
            });
        });
    });
};
categorySchema.post('findOneAndDelete', removeLinkedDocuments);

const Category = config.mongoose.model("category", categorySchema);
const Dish = config.mongoose.model("dish", dishSchema);


module.exports = {Category, Dish};
