const config = require('./../config/config');
const Schema = config.mongoose.Schema;

const categoryLikeSchema = new Schema({
        _user: {type: Schema.ObjectId, ref: 'user'},
        _category: {type: Schema.ObjectId, ref: 'category'},
    },
    {
        timestamps: {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt'
        }
    });
categoryLikeSchema.index({ _user: 1, _category: 1 }, { unique: true });

const CategoryLike = config.mongoose.model("categoryLike", categoryLikeSchema);
module.exports = CategoryLike;
