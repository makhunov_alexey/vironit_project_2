const config = require('./../config/config');
const Schema = config.mongoose.Schema;

const dishLikeSchema = new Schema({
        _user: {type: Schema.ObjectId, ref: 'user'},
        _dish: {type: Schema.ObjectId, ref: 'dish'},
    },
    {
        timestamps: {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt'
        }
    });
dishLikeSchema.index({ _user: 1, _dish: 1 }, { unique: true });

const DishLike = config.mongoose.model("dishLike", dishLikeSchema);
module.exports = DishLike;
