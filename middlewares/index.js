module.exports = {
    role: function(req, res, next) {
        if(req.user.role === 'Admin') {
            next();
        } else {
            req.flash('error', 'This site is now read only thanks to spam and trolls.');
            res.redirect('back');
        }
    },
}