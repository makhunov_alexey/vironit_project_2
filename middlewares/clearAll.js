const jwt = require('jsonwebtoken');
const config = require('../config/config');
const {Tokens} = require('../models/TokenModel');


module.exports = async (req, res, next) => {
    if(req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        req.token = req.headers.authorization.split(' ')[1];
        jwt.verify(req.token, config.jwt_encryption, async (err, decoded) => {
            if(err) {
                res.status(401).send("This token isn't signed");
            } else {
                const id = decoded.data.id;
                const result = await Tokens.findOneAndDelete({userid:id});
                next();
            }
        })
    }
    else res.status(401).send('You cant logout before logging in')
}