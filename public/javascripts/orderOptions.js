
function deleteOrder() {
    let dishesList = document.querySelector('#admin-orders-list')
    dishesList.addEventListener("click", function(event) {
        let target = event.target;
        let button = target.closest("button");

        if (!button) return;
        if(button.classList.contains("delete-order")) {
            let orderDeletionConfirmation = confirm(
                "Are you sure you want to delete this order?",
            );
            if (orderDeletionConfirmation) {
                HttpClient.deleteOrder(button).then(function(response) {
                    console.log(response)
                });
                location.reload();
            }
        }

    })
};
deleteOrder();
