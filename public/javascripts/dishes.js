function hideContent(contentIdArray) {
    contentIdArray.forEach(function(contentId) {
        let elem = document.querySelector("#" + contentId + "");
        elem.classList.add("hide-content");
    });
};



function deleteComment() {
    let dishesList = document.querySelector('.comments-container')
    dishesList.addEventListener("click", function(event) {
        let target = event.target;
        let button = target.closest("button");

        if (!button) return;
        if(button.classList.contains("delete-comment-button")) {
            console.log(button.id);
            let commentDeletionConfirmation = confirm(
                "Are you sure you want to delete this comment?",
            );
            if (commentDeletionConfirmation) {
                HttpClient.deleteComment(button).then(function(response) {
                    console.log(response)
                });
                location.reload();
            }
        }

    })
};
deleteComment();

function editComment() {
    let dishesList = document.querySelector('.comments-container');
    dishesList.addEventListener("click", function(event) {
        let target = event.target;
        let button = target.closest("button");

        if (!button) return;
        if(button.classList.contains("edit-comment-button")) {
            console.log(button.id);
            showHiddenContent([button.id+1]);
        }
        if(button.classList.contains("save-comment-button")) {

            showHiddenContent([button.id]);
        }

    })
};
editComment();



// function closeOrderForm() {
//     let closeOrderFormButton = document.querySelector('#close-order-form');
//     closeOrderFormButton.addEventListener('click', function () {
//         hideContent(["popup-background", "new-order-form"]);
//
//     })
//
//
//     console.log(closeOrderFormButton);
//     // fc-past
// }
// closeOrderForm();