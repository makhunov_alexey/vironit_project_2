

function deleteDish() {
    let dishesList = document.querySelector('#admin-dishes-table')
    dishesList.addEventListener("click", function(event) {
        let target = event.target;
        let button = target.closest("button");

        if (!button) return;
        if(button.classList.contains("delete-dish")) {
            let dishDeletionConfirmation = confirm(
                "Are you sure you want to delete this dish?",
            );
            if (dishDeletionConfirmation) {
                HttpClient.deleteDish(button).then(function(response) {
                    console.log(response)
                });
                location.reload();
            }
        }

    })
};
deleteDish();

function renameDish() {
    let dishTable = document.querySelector('#admin-dishes-table');
    let image = document.querySelector('#image');
    if (dishTable) {
        dishTable.addEventListener("click", function(event) {
            let target = event.target;
            let button = target.closest("button");
            if (!button) return;
            const dishId = getOriginId(button.id);

            const imgAdd = document.querySelector('.imageAdd');
            const photoAdd = document.querySelector('.photoAdd');
            const imgEdit = document.getElementsByClassName(dishId + 'imageEdit');

            if(button.classList.contains("edit-dish")) {
                imgAdd.setAttribute('id', '');
                imgEdit[0].attributes.id.nodeValue = 'image';
                readonlyToggle([dishId + 'dishTitle', dishId + 'dishDescription', dishId + 'dishCost']);
                showHiddenContent([button.id, dishId + 'saveDish', dishId + 'uploadPhoto']);

            }
            if(button.classList.contains("save-dish")) {
                if(image.classList.contains("picture-visability")) {
                    readonlyToggle([dishId + 'dishTitle', dishId + 'dishDescription', dishId + 'dishCost']);
                } else {
                    alert( "Please, select dish picture" );
                }

            }
        })
    }
};
renameDish();
