function showImage() {
    if (this.files && this.files[0]) {
        var obj = new FileReader();
        obj.onload = function (data) {
            var image = document.querySelector("#image");
            image.src = data.target.result;
            image.classList.add("picture-visability");
        }
        obj.readAsDataURL(this.files[0])
    }
}

function deleteCategory() {
    let categoriesList = document.querySelector('#admin-categories-list')
    if (categoriesList) {
        categoriesList.addEventListener("click", function(event) {
            let target = event.target;
            let button = target.closest("button");
            if (!button) return;
            const categoryId = getOriginId(button.id);
            if(button.classList.contains("delete-category")) {
                let categoryDeletionConfirmation = confirm(
                    "Are you sure you want to delete this category?",
                );
                if (categoryDeletionConfirmation) {
                    HttpClient.deleteCategory(button).then(function (response) {
                        console.log(response)
                        location.reload();
                    });
                    showHiddenContent([categoryId + 2]);
                }
            }
        })
    }
};
deleteCategory();

function renameCategory() {
    let categoriesList = document.querySelector('#admin-categories-list')
    if (categoriesList) {
        categoriesList.addEventListener("click", function(event) {
            let target = event.target;
            let button = target.closest("button");
            if (!button) return;
            const categoryId = getOriginId(button.id);
            if(button.classList.contains("edit-category")) {

                let categoryInput = document.getElementById(button.id + 1);
                readonlyToggle([button.id + 1]);
                showHiddenContent([button.id]);
                showHiddenContent([categoryId + 2]);

                if(categoryInput.hasAttribute('readonly')) {
                }
            }
            if(button.classList.contains("save-category")) {
                readonlyToggle([categoryId + 11]);
            }
        })
    }
};
renameCategory();

function deleteComment() {
    let dishesList = document.querySelector('#admin-comments-list')
    if(dishesList) {
        dishesList.addEventListener("click", function(event) {
            let target = event.target;
            let button = target.closest("button");

            if (!button) return;
            if(button.classList.contains("delete-order")) {
                let commentDeletionConfirmation = confirm(
                    "Are you sure you want to delete this comment?",
                );
                if (commentDeletionConfirmation) {
                    HttpClient.deleteComment(button).then(function(response) {
                        console.log(response)
                    });
                    location.reload();
                }
            }

        })
    }

};
deleteComment();


