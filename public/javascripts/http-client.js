const HttpClient = {
    deleteCategory: function (elemetn) {
        return fetch("http://localhost:3020/users/admin/deleteCategory/" + elemetn.id + "", {
            method: "DELETE",
            body: JSON.stringify(elemetn),
            headers: {"Content-Type": "application/json; charset=utf-8"}
        })
            .then (function() {
                alert('Category deleted');
            });
    },
    deleteDish: function (button) {
        return fetch("http://localhost:3020/users/admin/deleteDish/" + button.id + "", {
            method: "POST",
            body: JSON.stringify(button),
            headers: {"Content-Type": "application/json; charset=utf-8"}
        })
            .then (function() {
                alert('Dish deleted');
            });
    },
    deleteComment: function (element) {
        return fetch("http://localhost:3020/users/deleteComment/" + element.id + "", {
            method: "DELETE",
            body: JSON.stringify(element),
            headers: {"Content-Type": "application/json; charset=utf-8"}
        })
            .then (function() {
                alert('Category deleted');
            });
    },
    renameCategory: function (categoryId) {
        return fetch("http://localhost:3020/admin/renameCategory/" + categoryId + "", {
            method: "POST",
            body: JSON.stringify(categoryId),
            headers: {"Content-Type": "application/json; charset=utf-8"}
        })
            .then (function() {
                alert('Category renamed');
            });
    },
    deleteOrder: function (element) {
        return fetch("http://localhost:3020/users/deleteOrder/" + element.id + "", {
            method: "DELETE",
            body: JSON.stringify(element),
            headers: {"Content-Type": "application/json; charset=utf-8"}
        })
            .then (function() {
                alert('Order deleted');
            });
    },

}
