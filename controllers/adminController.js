const {Category} = require('../models/categoryModel');
const {Dish} = require('../models/categoryModel');
const DishLike = require('../models/dishLikeModel');
const User = require('../models/userModel');
const Order = require('../models/orderModel');
const Comment = require('../models/commentModel');
const jwt = require('jsonwebtoken');

module.exports.addCategory = async (req, res, next) => {
    const category = new Category({
        categoryName: req.body.categoryName
    });
    category
        .save()
        .then(result => {
            res.redirect('/users/admin/menuEdit');
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
};

module.exports.renameCategory = async (req, res, next) => {
    try {
        let categoryId = req.params.id;
        const category = await Category.findOne({_id: categoryId});
        category.categoryName = req.body.categoryName;
        await category.save();
        res.redirect("back");
    } catch (error) {
        console.log(error);
    }

};

module.exports.deleteCategory = async (req, res, next) => {
    let id = req.params.id;
    Category.findOneAndDelete({_id: id}, function (err) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        }
        return res.status(200).send();
    })
};

module.exports.addDish = async (req, res, next) => {
    if (req.file) {
        if (req.file.filename) {
            req.body.photo = req.file.filename;
        } else {
            req.body.photo = 'plate2.jpg';
        }
    }

    const categoryId = req.body.optionsRadios;
    const dishTitle = req.body.dishTitle;
    const dishDescription = req.body.dishDescription;
    const dishCost = req.body.dishCost;
    const photo = req.body.photo;


    const category = await Category.findOne({_id: categoryId});
    const dish = new Dish({
        _category: categoryId,
        itemTitle: dishTitle,
        itemDescription: dishDescription,
        itemCost: dishCost,
        image: photo
    });

    dish
        .save()
        .then(dish => {
            category.dishes.push(dish);
            category.save();
            res.redirect('back');
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
};

module.exports.renameDish = async (req, res, next) => {
    try {
        let dishId = req.params.id;

        const dish = await Dish.findOne({_id: dishId});
        dish.itemTitle = req.body.dishTitle;
        dish.itemDescription = req.body.dishDescription;
        dish.itemCost = req.body.dishCost;
        dish.image = req.file.filename;
        await dish.save();
        res.redirect("back");
    } catch (error) {
        console.log(error);
    }

};

module.exports.deleteDish = async (req, res, next) => {
    let id = req.params.id;
    Dish.findByIdAndDelete({_id:id},function(err){
        if (err){
            console.log(err);
            return res.status(500),send();
        }
        return res.status(200).send();
    });
};
