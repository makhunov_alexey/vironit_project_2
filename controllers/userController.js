const Order = require('../models/orderModel');
const {Dish} = require('../models/categoryModel');
const {Category} = require('../models/categoryModel');
const User = require('../models/userModel');
const Comment = require('../models/commentModel');
const DishLike = require('../models/dishLikeModel');
const CategoryLike = require('../models/categoryLikeModel');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const config = require('./../config/config');
// const appfunction = require('../app');
const dotenv = require('dotenv');
dotenv.config();

module.exports.verify = async (req, res, next) => {
    try {
        let user;
        let token = req.cookies["accessToken"];
        jwt.verify(token, config.tokens.secret, async function (err, decoded) {
            if (err) {
                const {email} = jwt.decode(token);
                const newUser = await User.findOne({'email': email});
                const newToken = jwt.sign(newUser.toJSON(), config.tokens.secret, {expiresIn: config.tokens.tokenLife});
                res.cookie("accessToken", newToken);
                // const user = {};
                const url = sendMessage(email, newToken);
                if (url) {
                    res.render('confirmed', {user: newUser});
                } else {
                    res.end("error");
                }
            }
            if (decoded) {
                user = decoded;
                const userUpdate = await User.findById(user._id);
                userUpdate.isConfirmed = 'true';
                const refreshToken = jwt.sign(userUpdate.toJSON(), config.tokens.refreshTokenSecret, {expiresIn: config.tokens.refreshTokenLife})
                const accessToken = jwt.sign(userUpdate.toJSON(), config.tokens.secret, {expiresIn: config.tokens.tokenLife});
                config.client.set(userUpdate._id, refreshToken);
                res.cookie("accessToken", accessToken);
                await userUpdate.save();
                res.redirect('/users/login');
            }
        });
    } catch (e) {
        res.render('login');
        console.log(e);
    }
};

module.exports.register = async (req, res, next) => {
    try {
        const result = req.body;
        if (result.error) {
            console.log("validate");
            res.render('register');
            return
        }

        const user = await User.findOne({'email': result.email})
        if (user) {
            res.render('register', {message: "This email is already in use, please try another one", user: user});
            return
        }

        if (result.password === result.confirmationPassword) {
            const hash = await bcrypt.hash(req.body.password, 10);
            result.email = req.body.email;
            result.password = hash;
            result.role = "User";
            result.isConfirmed = false;
            const newUser = await new User(result);
            await newUser.save();
            const accessToken = jwt.sign(newUser.toJSON(), config.tokens.secret, {expiresIn: config.tokens.tokenLife});
            res.cookie("accessToken", accessToken);

            // let token = req.cookies["accessToken"];
            const {_id} = jwt.decode(accessToken);
            const user = await User.findById(_id);

            const url = sendMessage(result.email, accessToken);
            if (url) {

                res.render('confirmed', {user: user});
            } else {
                res.end('error');
            }

        } else {
            console.log("password");
        }
    } catch (error) {
        next(error);
    }
};

async function sendMessage(email, token) {
    let link = "http://localhost:3020/users/verify?token=" + token;

    //Compose an email
    const output = `Hi there,
            <br/>
            Thank you for registering!
            <br/><br/>
            Please verify your email On the following page:
            <a href="${link}">Click here to verify</a>
            <br/><br/>
            Have a pleasant day.
            `;

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        port: 25,
        secure: false, // true for 465, false for other ports
        auth: {
            user: process.env.email, // generated ethereal user
            pass: process.env.Email_password  // generated ethereal password
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"Nodemailer Contact" <matvey.andreev2000@gmail.com>', // sender address
        to: email, // list of receivers
        subject: 'Node Contact Request', // Subject line
        text: 'Hello world?', // plain text body
        html: output // html body
    };

    transporter.sendMail(mailOptions, (error, responce) => {
        if (error) {
            console.log(error);
            return false;
        } else {
            console.log("Message sent: " + responce.message);
            return true;
        }
    })
}

module.exports.authenticate = async (req, res, next) => {
    try {
        const result = req.body;
        const user = await User.findOne({'email': result.email});
        if (user && user.isConfirmed) {
            const match = await bcrypt.compare(result.password, user.password);
            if (match) {
                const refreshToken = jwt.sign(user.toJSON(), config.tokens.refreshTokenSecret, {expiresIn: config.tokens.refreshTokenLife})

                config.client.set(user._id.toString(), refreshToken);
                console.log('Login: ' + user._id);
                const accessToken = jwt.sign(user.toJSON(), config.tokens.secret, {expiresIn: config.tokens.tokenLife});

                res.cookie("accessToken", accessToken);
                await user.save();
                res.redirect('/users/menu');
            }
            return
        } else {
            res.redirect('/users/login');
        }
    } catch (error) {
        next(error)
    }
};

module.exports.createOrder = async (req, res, next) => {
    try {
        const user = await User.findOne({_id: req.body.userId});
        const order = new Order({
            _user: req.body.userId,
            dishes: req.body.dishId,
            orderDate: req.body.orderDate
        });
        let dishes = order.dishes;
        dishes.forEach(async function (elem) {
            let dish = await Dish.findOne({_id: elem});
            dish.orders.push(order);
            dish.save();
        })
        user.orders.push(order);
        user.save();
        order.save();
        res.redirect('/users/calendar');
    } catch (error) {
        console.log(error);
    }
};

module.exports.deleteOrder = async (req, res, next) => {
    let id = req.params.id;
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const user = await User.findById(_id);
    let orderDishes = (await Order.findOne({_id: id})).dishes;
    orderDishes.forEach(async function (element) {
        let dish = await Dish.findOne({_id: element});
        dish.orders.splice(dish.orders.indexOf(id), 1);
        dish.save();
    });
    user.orders.splice(user.orders.indexOf(id), 1);
    user.save();
    Order.findOneAndDelete({_id: id}, function (err) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        }
        return res.status(200).send();
    })
};

module.exports.addComment = async (req, res, next) => {
    let dishId = req.params.id;
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const dish = await Dish.findOne({_id: dishId});
    const date = new Date();
    const user = await User.findById(_id);
    const comment = new Comment({
        _user: _id,
        _dish: dishId,
        commentBody: req.body.commentBody
    });
    comment
        .save()
        .then(comment => {
            dish.comments.push(comment);
            dish.save();
            user.comments.push(comment);
            user.save();
            res.redirect('back');
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        });
};

module.exports.updateComment = async (req, res, next) => {
    try {
        let commentId = req.params.id;
        const comment = await Comment.findOne({_id: commentId});
        comment.commentBody = req.body.commentBody;
        await comment.save();
        res.redirect("back");
    } catch (error) {
        console.log(error);
    }
};

module.exports.deleteComment = async (req, res, next) => {
    let id = req.params.id;
    const {_id} = jwt.decode(req.cookies["accessToken"]);
    const user = await User.findById(_id);
    let dish = await Dish.findOne({_id: (await Comment.findOne({_id: id}))._dish});
    dish.comments.splice(dish.comments.indexOf(id), 1);
    dish.save();
    user.comments.splice(user.comments.indexOf(id), 1);
    user.save();
    await Comment.findOneAndDelete({_id: id}, function (err) {
        if (err) {
            console.log(err);
            return res.status(500).send();
        }
        return res.status(200).send();
    });
};

module.exports.addDishLike = async (req, res, next) => {
    let dishId = req.params.id;
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const dish = await Dish.findOne({_id: dishId}).populate({path: 'dishLikes', select: '_user'});
    const user = await User.findById(_id);
    const dishLike = new DishLike({
        _user: _id,
        _dish: dishId
    });
    if (await DishLike.findOne({_user: dishLike._user, _dish: dishLike._dish}) === null) {
        dishLike.save();
        dish.dishLikes.push(dishLike);
        dish.save();
        user.dishLikes.push(dishLike);
        user.save();
        res.redirect('back');
    } else {
        let dishLikeDelete = await DishLike.findOne({_user: dishLike._user, _dish: dishLike._dish}); //id like

        let dishUpdate = await Dish.findOne({_id: dishLike._dish});     //блюдо
        dishUpdate.dishLikes.splice(dishUpdate.dishLikes.indexOf(dishLikeDelete._id), 1); //удаление
        dishUpdate.save();

        user.dishLikes.splice(user.dishLikes.indexOf(dishLikeDelete._id), 1);
        user.save();

        await DishLike.findOneAndDelete({_user: dishLike._user, _dish: dishLike._dish});
        res.redirect('back');
    }
};
module.exports.addCategoryLike = async (req, res, next) => {
    let categoryId = req.params.id;
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const category = await Category.findOne({_id: categoryId}).populate({path: 'categoryLikes', select: '_user'});
    const user = await User.findById(_id);
    const categoryLike = new CategoryLike({
        _user: _id,
        _category: categoryId
    });
    if (await CategoryLike.findOne({_user: categoryLike._user, _category: categoryLike._category}) === null) {
        categoryLike.save();
        category.categoryLikes.push(categoryLike);
        category.save();
        user.categoryLikes.push(categoryLike);
        user.save();
        res.redirect('back');
    } else {
        let categoryLikeDelete = await CategoryLike.findOne({
            _user: categoryLike._user,
            _category: categoryLike._category
        });

        let categoryUpdate = await Category.findOne({_id: categoryLike._category});     //блюдо
        categoryUpdate.categoryLikes.splice(categoryUpdate.categoryLikes.indexOf(categoryLikeDelete._id), 1); //удаление
        categoryUpdate.save();

        user.categoryLikes.splice(user.categoryLikes.indexOf(categoryLikeDelete._id), 1);
        user.save();

        await CategoryLike.findOneAndDelete({_user: categoryLike._user, _category: categoryLike._category})
        res.redirect('back');
    }
};

module.exports.get_user = (req) => {
    try {
        var token = req.cookies["accessToken"];
        var user =  jwt.verify(token, config.tokens.secret);
        return user;
    } catch (e) {
        return null;
    }
}


