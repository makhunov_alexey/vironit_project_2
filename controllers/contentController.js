const { Category } = require('../models/categoryModel');
const { Dish } = require('../models/categoryModel');
const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const Order = require('../models/orderModel');
const config = require('./../config/config');

module.exports.showDishes = async (req, res, next) => {
    let id = req.params.id;
    let token = req.cookies["accessToken"];
    const { _id } = jwt.decode(token);
    const user = await User.findById(_id);
    const dishes = await Dish.find({ _category: id })
        .populate({ path: 'comments', populate: { path: '_user', select: 'email' } })
        .populate({ path: 'dishLikes', select: '_user' })
        .populate({ path: '_category' });
    res.render("dishes", { dishes: dishes, user: user, userId: user._id, userRole: user.role });
};

module.exports.showDashBoard = async (req, res, next)=> {
    let token = req.cookies["accessToken"];
    const {_id} = jwt.decode(token);
    const user = await User.findById(_id);
    await jwt.verify(token, config.tokens.secret, async function (err, decoded) {
        if (err) {
            const user = await User.findById(_id);
            config.client.del(user._id.toString());
            res.clearCookie("accessToken");
            res.render('loginrefresh');
        }
        if (decoded) {
            const user = await jwt.verify(token, config.tokens.secret);
            if (user.isConfirmed) {
                let dishLike = await mostLikeDish();
                let dishOrder = await mostOrderDish(token);
                let dishOrderYesterday = await mostOrderedDishesForYesterday();
                res.render('dashboard',{dishLike: dishLike, dishOrder: dishOrder, dishOrderYesterday: dishOrderYesterday, user: user});
            }
        }
    });
};

async function mostLikeDish(){
    let dish = await Dish.find().populate({ path: '_category', select: 'categoryName'});
    let dishSortLike = dish.sort(function(a,b){
        if (a.dishLikes.length > b.dishLikes.length) {return -1;}
        if (a.dishLikes.length < b.dishLikes.length) {return 1;}
    return 0;
    });
    return dishSortLike;
}

async function mostOrderDish(token){
    const user = await jwt.verify(token, config.tokens.secret);
    const dishes = await Dish.find().select('_id');
    let rating = [];
    let sortedDish =[];
    for(el in dishes) {
        let number = await Order.find({_user: {$eq: user._id}}).select('dishes').count({dishes: dishes[el]._id});
        let dishRating = {
            dishId: dishes[el]._id,
            orderQuantity: number,
        };
        rating.push(dishRating);
    }
        rating.sort(function (a, b) {
            let keyA = a.orderQuantity,
                keyB = b.orderQuantity;
            if (keyA > keyB) {
                return -1;
            }
            if (keyA < keyB) {
                return 1;
            }
            return 0;
        });

    for(element in rating){
        sortedDish.push(await Dish.findOne({_id:rating[element].dishId}).populate({ path: '_category', select: 'categoryName'}))
    }
    return sortedDish;
}

async function mostOrderedDishesForYesterday() {
    let startDate = new Date();
    let endDate = new Date();
    startDate.setHours(startDate.getHours() - 24);
    const dishes = await Dish.find().select('_id');
    let rating = [];
    let sortedDish =[];
    for(el in dishes){let number = await Order.find({ $and: [ { orderDate: { $gte: startDate } }, { orderDate: { $lt: endDate } }]}).select('dishes').count({ dishes: dishes[el]._id });
            let dishRating = {
                dishId: dishes[el]._id,
                orderQuantity: number,
            };
            rating.push(dishRating);
    }

    rating.sort(function(a,b){
        let keyA = a.orderQuantity,
            keyB = b.orderQuantity;
        if (keyA > keyB) { return -1; }
        if (keyA < keyB) { return 1; }
        return 0;
    });

    for(element in rating){
        sortedDish.push(await Dish.findOne({_id:rating[element].dishId}).populate({ path: '_category', select: 'categoryName'}))
    }
    return sortedDish;
}