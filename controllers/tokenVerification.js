const jwt = require('jsonwebtoken');
const config = require('./../config/config');
const User = require('../models/userModel');

module.exports = async (req, res, next) => {
    let token = req.cookies["accessToken"];
    if (token) {
        await jwt.verify(token, config.tokens.secret, async function (err, decoded) {
            if(decoded) {
                const {_id} = jwt.decode(token);
                const checkUser = await User.findById(_id);
                config.client.get(_id.toString(), async function (err, refreshToken) {
                    await jwt.verify(refreshToken, config.tokens.refreshTokenSecret, async function (err, decoded) {
                        if (err) {
                            res.redirect('/users/login');
                        }
                        else {
                            const refreshToken = await jwt.sign(checkUser.toJSON(), config.tokens.refreshTokenSecret, {expiresIn: config.tokens.refreshTokenLife})
                            config.client.set(checkUser._id.toString(), refreshToken);
                            const accessToken = await jwt.sign(checkUser.toJSON(), config.tokens.secret, {expiresIn: config.tokens.tokenLife});

                            checkUser.save();
                            res.cookie("accessToken", accessToken);
                            res.redirect(req.originalUrl);
                        }
                    })
                });
            }
        });
    } else {
        return res.status(403).send({
            "error": true,
            "message": 'No token provided.'
        });
    }
}